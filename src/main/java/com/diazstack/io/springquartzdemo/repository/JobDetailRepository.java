package com.diazstack.io.springquartzdemo.repository;

import com.diazstack.io.springquartzdemo.entity.QrtzJobDetailsEntity;
import com.diazstack.io.springquartzdemo.entity.QrtzJobDetailsEntityPK;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface JobDetailRepository extends JpaRepository<QrtzJobDetailsEntity, QrtzJobDetailsEntityPK> {

    Optional<QrtzJobDetailsEntity> findByJobName(String jobName);
}
