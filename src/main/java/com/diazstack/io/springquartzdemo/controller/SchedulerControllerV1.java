package com.diazstack.io.springquartzdemo.controller;

import com.diazstack.io.springquartzdemo.entity.QrtzJobDetailsEntity;
import com.diazstack.io.springquartzdemo.model.JobRequest;
import com.diazstack.io.springquartzdemo.model.JobResponse;
import com.diazstack.io.springquartzdemo.service.SchedulerService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.quartz.JobDetail;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.text.ParseException;

@RestController
@RequestMapping("/v1/jobs")
@Slf4j
@RequiredArgsConstructor
public class SchedulerControllerV1 {

    final private SchedulerService schedulerService;

    @PostMapping
    public ResponseEntity<JobResponse> createJob(@Valid @RequestBody JobRequest jobRequest) {

        JobDetail jobDetail = schedulerService.buildJobDetail(jobRequest);
        try {
            if (schedulerService.checkJobDetailExists(jobRequest.getPhoneMAC(), jobRequest.getLocation())
                    & schedulerService.checkTriggerExists(jobRequest.getPhoneMAC(), jobRequest.getLocation())) {
                throw new ResponseStatusException(HttpStatus.ACCEPTED, "Already Exists");
            }

            Trigger trigger = schedulerService.buildCronTrigger(jobRequest, jobDetail);

            schedulerService.saveJob(jobDetail, trigger);

            JobResponse jobResponse = new JobResponse(jobDetail.getKey().getName(),
                    jobDetail.getKey().getGroup());

            return ResponseEntity.status(HttpStatus.CREATED).body(jobResponse);

        } catch (SchedulerException e) {
            log.error(jobRequest.toString() + jobDetail.toString(), e);
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
        } catch (ParseException e) {
            log.error(jobRequest.toString(), e);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Bad parameters in request");
        }
    }

    @GetMapping(params = "phoneMac")
    public ResponseEntity<JobResponse> findJobDetail(@RequestParam String phoneMac) {

        QrtzJobDetailsEntity jb = schedulerService.findJobDetail(phoneMac);
        JobResponse jobResponse = new JobResponse(jb.getJobName(),
                jb.getJobGroup());
        return ResponseEntity.ok(jobResponse);
    }

    @DeleteMapping
    public ResponseEntity<JobResponse> deleteAll() {
        schedulerService.deleteAll();
        return ResponseEntity.ok().build();
    }
}
