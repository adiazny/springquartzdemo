package com.diazstack.io.springquartzdemo.controller;

import com.diazstack.io.springquartzdemo.service.Producer;
import com.diazstack.io.springquartzdemo.worker.WorkerTask;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/message")
public class MessageController {

    private static final Logger log = LoggerFactory.getLogger(MessageController.class);

    @Autowired
    private Producer producer;

    @PostMapping(consumes = "application/json")
    public void postMessage(@RequestBody WorkerTask task){
        producer.send(task);
    }

}
