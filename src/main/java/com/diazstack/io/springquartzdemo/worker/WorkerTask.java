package com.diazstack.io.springquartzdemo.worker;

public class WorkerTask {

    private String name;
    private Integer priority;
    private String operator;

    public WorkerTask(String name, Integer priority, String operator) {
        this.name = name;
        this.priority = priority;
        this.operator = operator;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    @Override
    public String toString() {
        return "WorkerTask{" +
                "name='" + name + '\'' +
                ", priority=" + priority +
                ", operator='" + operator + '\'' +
                '}';
    }
}
