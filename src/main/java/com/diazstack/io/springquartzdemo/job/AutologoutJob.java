package com.diazstack.io.springquartzdemo.job;

import com.diazstack.io.springquartzdemo.service.Producer;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
public class AutologoutJob implements Job {


    @Autowired
    Producer producer;

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        System.out.println("Hello from job execution of " +jobExecutionContext.getJobDetail().getKey().toString()
                +  " fired @ " + LocalDateTime.now());
//        WorkerTask workerTask = new WorkerTask("Alan " + LocalDateTime.now(), 234, "Op1");

        jobExecutionContext.getJobDetail().getKey();

//        producer.send(workerTask);
    }
}
