package com.diazstack.io.springquartzdemo.exception;

public class SchedulerError extends RuntimeException {

    public SchedulerError(String msg){
        super(msg);
    }

}
