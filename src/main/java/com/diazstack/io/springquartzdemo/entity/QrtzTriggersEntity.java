package com.diazstack.io.springquartzdemo.entity;

import javax.persistence.*;
import java.sql.Blob;

@SuppressWarnings("ALL")
@Entity
@Table(name = "QRTZ_TRIGGERS", schema = "PUBLIC", catalog = "TESTDB")
@IdClass(QrtzTriggersEntityPK.class)
public class QrtzTriggersEntity {
    private String schedName;
    private String triggerName;
    private String triggerGroup;
    private String jobName;
    private String jobGroup;
    private String description;
    private Long nextFireTime;
    private Long prevFireTime;
    private Integer priority;
    private String triggerState;
    private String triggerType;
    private long startTime;
    private Long endTime;
    private String calendarName;
    private Short misfireInstr;
    private Blob jobData;
//    private QrtzCronTriggersEntity qrtzCronTriggers;
    private QrtzJobDetailsEntity qrtzJobDetails;


    @Id
    @Column(name = "SCHED_NAME")
    public String getSchedName() {
        return schedName;
    }

    public void setSchedName(String schedName) {
        this.schedName = schedName;
    }

    @Id
    @Column(name = "TRIGGER_NAME")
    public String getTriggerName() {
        return triggerName;
    }

    public void setTriggerName(String triggerName) {
        this.triggerName = triggerName;
    }

    @Id
    @Column(name = "TRIGGER_GROUP")
    public String getTriggerGroup() {
        return triggerGroup;
    }

    public void setTriggerGroup(String triggerGroup) {
        this.triggerGroup = triggerGroup;
    }

    @Column(name = "JOB_NAME")
    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }


    @Column(name = "JOB_GROUP")
    public String getJobGroup() {
        return jobGroup;
    }

    public void setJobGroup(String jobGroup) {
        this.jobGroup = jobGroup;
    }


    @Column(name = "DESCRIPTION")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    @Column(name = "NEXT_FIRE_TIME")
    public Long getNextFireTime() {
        return nextFireTime;
    }

    public void setNextFireTime(Long nextFireTime) {
        this.nextFireTime = nextFireTime;
    }


    @Column(name = "PREV_FIRE_TIME")
    public Long getPrevFireTime() {
        return prevFireTime;
    }

    public void setPrevFireTime(Long prevFireTime) {
        this.prevFireTime = prevFireTime;
    }


    @Column(name = "PRIORITY")
    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }


    @Column(name = "TRIGGER_STATE")
    public String getTriggerState() {
        return triggerState;
    }

    public void setTriggerState(String triggerState) {
        this.triggerState = triggerState;
    }


    @Column(name = "TRIGGER_TYPE")
    public String getTriggerType() {
        return triggerType;
    }

    public void setTriggerType(String triggerType) {
        this.triggerType = triggerType;
    }


    @Column(name = "START_TIME")
    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }


    @Column(name = "END_TIME")
    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }


    @Column(name = "CALENDAR_NAME")
    public String getCalendarName() {
        return calendarName;
    }

    public void setCalendarName(String calendarName) {
        this.calendarName = calendarName;
    }


    @Column(name = "MISFIRE_INSTR")
    public Short getMisfireInstr() {
        return misfireInstr;
    }

    public void setMisfireInstr(Short misfireInstr) {
        this.misfireInstr = misfireInstr;
    }


    @Column(name = "JOB_DATA")
    public Blob getJobData() {
        return jobData;
    }

    public void setJobData(Blob jobData) {
        this.jobData = jobData;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        QrtzTriggersEntity that = (QrtzTriggersEntity) o;

        if (startTime != that.startTime) return false;
        if (schedName != null ? !schedName.equals(that.schedName) : that.schedName != null) return false;
        if (triggerName != null ? !triggerName.equals(that.triggerName) : that.triggerName != null) return false;
        if (triggerGroup != null ? !triggerGroup.equals(that.triggerGroup) : that.triggerGroup != null) return false;
        if (jobName != null ? !jobName.equals(that.jobName) : that.jobName != null) return false;
        if (jobGroup != null ? !jobGroup.equals(that.jobGroup) : that.jobGroup != null) return false;
        if (description != null ? !description.equals(that.description) : that.description != null) return false;
        if (nextFireTime != null ? !nextFireTime.equals(that.nextFireTime) : that.nextFireTime != null) return false;
        if (prevFireTime != null ? !prevFireTime.equals(that.prevFireTime) : that.prevFireTime != null) return false;
        if (priority != null ? !priority.equals(that.priority) : that.priority != null) return false;
        if (triggerState != null ? !triggerState.equals(that.triggerState) : that.triggerState != null) return false;
        if (triggerType != null ? !triggerType.equals(that.triggerType) : that.triggerType != null) return false;
        if (endTime != null ? !endTime.equals(that.endTime) : that.endTime != null) return false;
        if (calendarName != null ? !calendarName.equals(that.calendarName) : that.calendarName != null) return false;
        if (misfireInstr != null ? !misfireInstr.equals(that.misfireInstr) : that.misfireInstr != null) return false;
        if (jobData != null ? !jobData.equals(that.jobData) : that.jobData != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = schedName != null ? schedName.hashCode() : 0;
        result = 31 * result + (triggerName != null ? triggerName.hashCode() : 0);
        result = 31 * result + (triggerGroup != null ? triggerGroup.hashCode() : 0);
        result = 31 * result + (jobName != null ? jobName.hashCode() : 0);
        result = 31 * result + (jobGroup != null ? jobGroup.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (nextFireTime != null ? nextFireTime.hashCode() : 0);
        result = 31 * result + (prevFireTime != null ? prevFireTime.hashCode() : 0);
        result = 31 * result + (priority != null ? priority.hashCode() : 0);
        result = 31 * result + (triggerState != null ? triggerState.hashCode() : 0);
        result = 31 * result + (triggerType != null ? triggerType.hashCode() : 0);
        result = 31 * result + (int) (startTime ^ (startTime >>> 32));
        result = 31 * result + (endTime != null ? endTime.hashCode() : 0);
        result = 31 * result + (calendarName != null ? calendarName.hashCode() : 0);
        result = 31 * result + (misfireInstr != null ? misfireInstr.hashCode() : 0);
        result = 31 * result + (jobData != null ? jobData.hashCode() : 0);
        return result;
    }

//    @OneToOne(mappedBy = "qrtzTriggers", cascade = CascadeType.ALL)
//    public QrtzCronTriggersEntity getQrtzCronTriggers() {
//        return qrtzCronTriggers;
//    }
//
//    public void setQrtzCronTriggers(QrtzCronTriggersEntity qrtzCronTriggers) {
//        this.qrtzCronTriggers = qrtzCronTriggers;
//    }

    @ManyToOne
    @JoinColumns({
            @JoinColumn(name = "SCHED_NAME", referencedColumnName = "SCHED_NAME", nullable = false, insertable = false, updatable = false),
            @JoinColumn(name = "JOB_NAME", referencedColumnName = "JOB_NAME", nullable = false, insertable = false, updatable = false),
            @JoinColumn(name = "JOB_GROUP", referencedColumnName = "JOB_GROUP", nullable = false, insertable = false, updatable = false)
    })
    public QrtzJobDetailsEntity getQrtzJobDetails() {
        return qrtzJobDetails;
    }

    public void setQrtzJobDetails(QrtzJobDetailsEntity qrtzJobDetails) {
        this.qrtzJobDetails = qrtzJobDetails;
    }

}
