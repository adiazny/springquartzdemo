package com.diazstack.io.springquartzdemo.entity;

import javax.persistence.*;
import java.sql.Blob;
import java.util.Collection;

@Entity
@Table(name = "QRTZ_JOB_DETAILS", schema = "PUBLIC", catalog = "TESTDB")
@IdClass(QrtzJobDetailsEntityPK.class)
public class QrtzJobDetailsEntity {
    private String schedName;
    private String jobName;
    private String jobGroup;
    private String description;
    private String jobClassName;
    private boolean isDurable;
    private boolean isNonconcurrent;
    private boolean isUpdateData;
    private boolean requestsRecovery;
    private Blob jobData;
    private Collection<QrtzTriggersEntity> qrtzTriggers;

    @Id
    @Column(name = "SCHED_NAME")
    public String getSchedName() {
        return schedName;
    }

    public void setSchedName(String schedName) {
        this.schedName = schedName;
    }

    @Id
    @Column(name = "JOB_NAME")
    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    @Id
    @Column(name = "JOB_GROUP")
    public String getJobGroup() {
        return jobGroup;
    }

    public void setJobGroup(String jobGroup) {
        this.jobGroup = jobGroup;
    }


    @Column(name = "DESCRIPTION")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    @Column(name = "JOB_CLASS_NAME")
    public String getJobClassName() {
        return jobClassName;
    }

    public void setJobClassName(String jobClassName) {
        this.jobClassName = jobClassName;
    }


    @Column(name = "IS_DURABLE")
    public boolean isDurable() {
        return isDurable;
    }

    public void setDurable(boolean durable) {
        isDurable = durable;
    }


    @Column(name = "IS_NONCONCURRENT")
    public boolean isNonconcurrent() {
        return isNonconcurrent;
    }

    public void setNonconcurrent(boolean nonconcurrent) {
        isNonconcurrent = nonconcurrent;
    }


    @Column(name = "IS_UPDATE_DATA")
    public boolean isUpdateData() {
        return isUpdateData;
    }

    public void setUpdateData(boolean updateData) {
        isUpdateData = updateData;
    }


    @Column(name = "REQUESTS_RECOVERY")
    public boolean isRequestsRecovery() {
        return requestsRecovery;
    }

    public void setRequestsRecovery(boolean requestsRecovery) {
        this.requestsRecovery = requestsRecovery;
    }


    @Column(name = "JOB_DATA")
    public Blob getJobData() {
        return jobData;
    }

    public void setJobData(Blob jobData) {
        this.jobData = jobData;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        QrtzJobDetailsEntity that = (QrtzJobDetailsEntity) o;

        if (isDurable != that.isDurable) return false;
        if (isNonconcurrent != that.isNonconcurrent) return false;
        if (isUpdateData != that.isUpdateData) return false;
        if (requestsRecovery != that.requestsRecovery) return false;
        if (schedName != null ? !schedName.equals(that.schedName) : that.schedName != null) return false;
        if (jobName != null ? !jobName.equals(that.jobName) : that.jobName != null) return false;
        if (jobGroup != null ? !jobGroup.equals(that.jobGroup) : that.jobGroup != null) return false;
        if (description != null ? !description.equals(that.description) : that.description != null) return false;
        if (jobClassName != null ? !jobClassName.equals(that.jobClassName) : that.jobClassName != null) return false;
        if (jobData != null ? !jobData.equals(that.jobData) : that.jobData != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = schedName != null ? schedName.hashCode() : 0;
        result = 31 * result + (jobName != null ? jobName.hashCode() : 0);
        result = 31 * result + (jobGroup != null ? jobGroup.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (jobClassName != null ? jobClassName.hashCode() : 0);
        result = 31 * result + (isDurable ? 1 : 0);
        result = 31 * result + (isNonconcurrent ? 1 : 0);
        result = 31 * result + (isUpdateData ? 1 : 0);
        result = 31 * result + (requestsRecovery ? 1 : 0);
        result = 31 * result + (jobData != null ? jobData.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "qrtzJobDetails",cascade = CascadeType.ALL)
    public Collection<QrtzTriggersEntity> getQrtzTriggers() {
        return qrtzTriggers;
    }

    public void setQrtzTriggers(Collection<QrtzTriggersEntity> qrtzTriggers) {
        this.qrtzTriggers = qrtzTriggers;
    }
}
