package com.diazstack.io.springquartzdemo.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Profile("rabbit")
@Configuration
public class RabbitMQConfig {

    private static final Logger log = LoggerFactory.getLogger(RabbitMQConfig.class);

    @Value("${queueName}")
    private String queueName;

    @Value("${exchangeName}")
    private String exchangeName;

    @Value("${routingKey}")
    private String routingKey;

    @Bean
    public CachingConnectionFactory connectionFactory() {
        CachingConnectionFactory conn = new CachingConnectionFactory();
        conn.setUsername("guest");
        conn.setPassword("guest");
        conn.setVirtualHost("/");
        conn.setHost("localhost");
        conn.setPort(5672);
        return conn;
    }

    @Bean
    public Queue queue() {
        return new Queue(queueName, true);

    }

    @Bean
    public DirectExchange exchange() {
        return new DirectExchange(exchangeName);
    }

    @Bean
    public Binding bind() {
        return BindingBuilder.bind(queue()).to(exchange()).with(routingKey);
    }


    @Bean
    public RabbitTemplate rabbitTemplate(CachingConnectionFactory connectionFactory) {
        RabbitTemplate rabbitTemplate = new RabbitTemplate();
        rabbitTemplate.setMessageConverter(jsonConverter());
        rabbitTemplate.setConnectionFactory(connectionFactory);
        rabbitTemplate.setExchange(exchangeName);
        rabbitTemplate.setDefaultReceiveQueue(queueName);

        return rabbitTemplate;
    }

    @Bean
    public SimpleMessageListenerContainer simpleMessageListenerContainer(CachingConnectionFactory connectionFactory) {
        SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();

        try {
            container.setConnectionFactory(connectionFactory);
            container.setQueueNames(queueName);
            container.setAcknowledgeMode(AcknowledgeMode.MANUAL);
            container.setAutoStartup(true);
            container.setPrefetchCount(5);
            container.setConcurrentConsumers(1);
            container.setMessageListener(new MessageListenerAdapter(consumer(), jsonConverter()));

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            log.info("inside listener finally");
        }

        return container;
    }

    @Bean
    public MessageListener consumer() {
        return (Message message) -> {
            byte[] body = message.getBody();
            log.info("Consumed message: " + new String(body));
            log.info("Message properties: " + message.getMessageProperties());
        };
    }

    @Bean
    public Jackson2JsonMessageConverter jsonConverter() {
        return new Jackson2JsonMessageConverter();
    }

}
