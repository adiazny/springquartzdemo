package com.diazstack.io.springquartzdemo.service;

import com.diazstack.io.springquartzdemo.entity.QrtzJobDetailsEntity;
import com.diazstack.io.springquartzdemo.exception.SchedulerError;
import com.diazstack.io.springquartzdemo.job.AutologoutJob;
import com.diazstack.io.springquartzdemo.model.JobRequest;
import com.diazstack.io.springquartzdemo.repository.JobDetailRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.quartz.*;
import org.springframework.scheduling.quartz.CronTriggerFactoryBean;
import org.springframework.scheduling.quartz.JobDetailFactoryBean;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.util.Date;
import java.util.Optional;

@Service
@Slf4j
@RequiredArgsConstructor
public class SchedulerService {

    private final Scheduler scheduler;
    private final JobDetailRepository jobDetailRepository;

    public JobDetail buildJobDetail(JobRequest jobRequest) {

        JobDetailFactoryBean jobDetailFactory = new JobDetailFactoryBean();

        jobDetailFactory.setJobClass(AutologoutJob.class);
        jobDetailFactory.setName(jobRequest.getPhoneMAC());
        jobDetailFactory.setGroup(jobRequest.getLocation());
        jobDetailFactory.setDescription(jobRequest.getLocation());
        jobDetailFactory.setDurability(true);

        jobDetailFactory.afterPropertiesSet();

        return jobDetailFactory.getObject();
    }

    public Trigger buildCronTrigger(JobRequest jobRequest, JobDetail jobDetail) throws ParseException {

        CronTriggerFactoryBean cronTriggerFactoryBean = new CronTriggerFactoryBean();

        cronTriggerFactoryBean.setJobDetail(jobDetail);
        cronTriggerFactoryBean.setName(jobRequest.getPhoneMAC());
        cronTriggerFactoryBean.setGroup(jobRequest.getLocation());
        cronTriggerFactoryBean.getJobDataMap().put("cucmIP", jobRequest.getCucmIP());
        cronTriggerFactoryBean.getJobDataMap().put("requestType", jobRequest.getRequestType());
        cronTriggerFactoryBean.setCronExpression(jobRequest.getCronString());

        cronTriggerFactoryBean.afterPropertiesSet();
        ;
        log.info("Building CronTrigger {} ", cronTriggerFactoryBean.getObject());
        return cronTriggerFactoryBean.getObject();
    }

    public Date saveJob(JobDetail jobDetail, Trigger trigger) throws SchedulerException {
        return scheduler.scheduleJob(jobDetail, trigger);
    }

    public QrtzJobDetailsEntity findJobDetail(String phoneMac) {
        Optional<QrtzJobDetailsEntity> jb = jobDetailRepository.findByJobName(phoneMac);
        return jb.get();
    }

    public boolean checkJobDetailExists(String phoneMac, String location) {

        try {
            return scheduler.checkExists(new JobKey(phoneMac, location));
        } catch (SchedulerException e) {
            throw new SchedulerError(e.getMessage());
        }
    }

    public boolean checkTriggerExists(String phoneMac, String location) {
        try {
            return scheduler.checkExists(new TriggerKey(phoneMac, location));
        } catch (SchedulerException e) {
            throw new SchedulerError(e.getMessage());
        }
    }

    public void deleteAll() {
        jobDetailRepository.deleteAll();
    }
}
