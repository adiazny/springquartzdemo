package com.diazstack.io.springquartzdemo.service;

import com.diazstack.io.springquartzdemo.worker.WorkerTask;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class Producer {

    private static final Logger log = LoggerFactory.getLogger(Producer.class);

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Value("${exchangeName}")
    private String exchange;

    @Value("${routingKey}")
    private String routingKey;

    public void send(WorkerTask task) {
        rabbitTemplate.convertAndSend(exchange, routingKey, task);
        log.info("Message generated as follows: " + task);
    }
}
