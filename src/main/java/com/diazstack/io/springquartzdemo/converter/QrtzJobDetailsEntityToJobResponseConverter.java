package com.diazstack.io.springquartzdemo.converter;

import com.diazstack.io.springquartzdemo.entity.QrtzJobDetailsEntity;
import com.diazstack.io.springquartzdemo.model.JobResponse;
import org.springframework.core.convert.converter.Converter;

public class QrtzJobDetailsEntityToJobResponseConverter implements Converter<QrtzJobDetailsEntity, JobResponse> {

    @Override
    public JobResponse convert(QrtzJobDetailsEntity qrtzJobDetailsEntity) {

        JobResponse jobResponse = new JobResponse();
        jobResponse.setLocation(qrtzJobDetailsEntity.getJobGroup());
        jobResponse.setPhoneMAC(qrtzJobDetailsEntity.getJobName());
        jobResponse.setTriggers(qrtzJobDetailsEntity.getQrtzTriggers());

        return null;
    }
}
