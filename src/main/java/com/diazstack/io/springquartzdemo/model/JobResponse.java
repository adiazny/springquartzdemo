package com.diazstack.io.springquartzdemo.model;

import com.diazstack.io.springquartzdemo.entity.QrtzTriggersEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Collection;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class JobResponse {

    private String phoneMAC;
    private String location;
    private Collection<QrtzTriggersEntity> triggers;

    public JobResponse(String phoneMAC, String location) {
        this.phoneMAC = phoneMAC;
        this.location = location;
    }
}
