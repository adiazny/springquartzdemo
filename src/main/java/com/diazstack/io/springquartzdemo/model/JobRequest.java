package com.diazstack.io.springquartzdemo.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.time.ZoneId;

@Data
public class JobRequest {

    @NotNull
    private String phoneMAC;

    @NotNull
    private String location;

    @NotNull
    private String requestType;

    @NotNull
    private String cucmIP;

    private String description;

    @NotNull
    private String cronString;

    public void setPhoneMAC(String phoneMAC) {
        this.phoneMAC = phoneMAC.toUpperCase();
    }

    public void setLocation(String location) {
        this.location = location.toLowerCase();
    }

}
